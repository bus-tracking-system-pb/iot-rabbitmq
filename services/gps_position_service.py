# GPS Position Fare Service

from nameko.rpc import rpc, RpcProxy

class GpsValue:
    name = "device_gps_pos"

    # mock
    @rpc
    def get_value(self, device_id):
        return  {'gps':{
                    'latitute':-43.3434,
                    'longitute':32.3232,
                    'attitude':432.3423}
                }
        