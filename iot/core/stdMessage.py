
# IoT-RabbitMQ API Packege

import configparser
import logging
from nameko.standalone.rpc import ClusterRpcProxy

logger = logging.getLogger(__name__)
CONFIG = {'AMQP_URI': "amqp://guest:guest@localhost:5672"}

class IoTMsg:
    """
    Parameters
    ----------

    Returns
    -------

    Raises
    ------
    """

    def __init__(self):
        self.header = None
        self.body = None
        self.gps_coord = {}
        self.response = {}
        self.actions_list = ["ticket_value",
                            "device_gps_pos",
                            "get_next_station"]
        self.stdDict = {
                    "header": {
                        "action": "",
                        "etag": "",
                        "code": "",
                        "document_datetime_init": ""
                    },
                    "body": {},
                    "api_timespan": ""}
        self.fareDict = {
                    "Normal": None,
                    "Student": None,
                    "Elderly":None}
        self.services = ClusterRpcProxy(CONFIG)
        self.bus_info = {}

    def _load_config(self):
        """
        Parameters
        ----------
        config: load api.cfg

        Returns
        -------
        Configuration Parameters:
            CLOUD:
                postgreSQL_host: string
                    hostname
                postgreSQL_port: int
                    postegreSQL Port
                postgreSQL_user: string
                    user name
                postgreSQL_pass: string
                    password        
        Raises
        ------
        Erro to read the file
        """
        config = configparser.ConfigParser()
        config.read('./api.cfg')

        self.postgreSQL_host = float(config['CLOUD']['postgreSQL_host'])
        self.postgreSQL_port = float(config['CLOUD']['postgreSQL_port'])
        self.postgreSQL_user = float(config['CLOUD']['postgreSQL_user'])
        self.postgreSQL_pass = float(config['CLOUD']['postgreSQL_pass'])
